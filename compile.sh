#!/bin/bash

babel --modules common ./lib/api.js > ./dist/api.js && echo "api.js OK"
babel --modules common ./lib/index.js > ./dist/index.js && echo "index.js OK"
