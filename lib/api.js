require('es6-promise').polyfill();
require('isomorphic-fetch');

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const error = new Error(response.statusText);
  error.err = response.statusText;
  error.status = response.status;
  error.response = response;
  return Promise.reject(error);
}

function parseJSON(response) {
  if (response.status === 204) {
    return Promise.resolve({ json: {}, response });
  }
  return response.json().then(json => ({ json: json.result, response }));
}

function handleStandardError(err) {
  if (err instanceof SyntaxError) {
    return { err: 'Invalid JSON returned. ' };
  }
  return Promise.reject(err);
}

export default class Api {
  constructor(config, accesstoken='') {
    this.config = config;
    this.accesstoken = accesstoken;
  }

  _url(uri) {
    return encodeURI(this.config.host + uri);
  }

  _headers() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': this.accesstoken
    };
  }

  _fetch(uri, options={}) {
    const headers = Object.assign(options.headers || {}, this._headers());
    return fetch(this._url(uri), Object.assign(options, { headers }))
      .then(checkStatus)
      .then(parseJSON)
      .catch(handleStandardError);
  }

  get(uri) {
    return this._fetch(uri);
  }

  post(uri, body) {
    return this._fetch(uri, {
      method: 'post',
      body: JSON.stringify(body),
    });
  }

  put(uri, body) {
    return this._fetch(uri, {
      method: 'put',
      body: JSON.stringify(body),
    });
  }

  delete(uri) {
    return this._fetch(uri, {
      method: 'delete',
    });
  }

  createUser(props) {
    return this.post('/users', props);
  }

  authenticate(userid, password) {
    return this.post('/tokens/' + userid, { password });
  }

  checkAccessToken(username, accesstoken) {
    return this.post('/tokens/validate', { username, accesstoken });
  }
}
